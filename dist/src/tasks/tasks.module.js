"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TasksModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const tasks_service_1 = require("./services/tasks.service");
const tasks_controller_1 = require("./controllers/tasks.controller");
const task_entity_1 = require("./entities/task.entity");
const Equipos_entity_1 = require("./entities/Equipos.entity");
const actividades_entity_1 = require("./entities/actividades.entity");
const actividades_service_1 = require("./services/actividades.service");
const Equipos_service_1 = require("./services/Equipos.service");
const Equipos_controller_1 = require("./controllers/Equipos.controller");
const actividades_controller_1 = require("./controllers/actividades.controller");
let TasksModule = class TasksModule {
};
TasksModule = __decorate([
    (0, common_1.Module)({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([task_entity_1.Task, Equipos_entity_1.Equipos, actividades_entity_1.actividades])
        ],
        providers: [tasks_service_1.TasksService, Equipos_service_1.EquiposService, actividades_service_1.actividadesService],
        controllers: [tasks_controller_1.TasksController, Equipos_controller_1.EquiposController, actividades_controller_1.actividadesController]
    })
], TasksModule);
exports.TasksModule = TasksModule;
//# sourceMappingURL=tasks.module.js.map